import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {CommonModule} from '@angular/common';

import {HeaderNavComponent} from './components';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
    ],
    declarations: [
        HeaderNavComponent,
    ],
    exports: [
        HeaderNavComponent,
    ]
})

export class HeaderNavModule {}
