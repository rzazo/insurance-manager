import autobind from "autobind-decorator";
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {faStar} from '@fortawesome/free-solid-svg-icons';

import {IModalConfig} from "../interfaces";
import {DEFAULT_HEADERS} from "../constants";
import {FavouritesModalContentComponent} from "../../../elements/favourites-modal-content/components/favourites-modal-content.component";

@Component({
    selector: 'pre-header',
    templateUrl: './pre-header.component.html',
    styleUrls: ['./pre-header.component.scss']
})

export class PreHeaderComponent {
    @Input() public model;

    @Output() returnFavs: EventEmitter<any> = new EventEmitter();

    public iconSearch = faStar;
    public modalContentComponent = FavouritesModalContentComponent;
    public modalRef: BsModalRef;
    public initialState: IModalConfig = {
        title: 'Manager favourites list',
        class: 'modal-lg',
    };

    constructor(private modalService: BsModalService) {}

    public openModal() {
        this.initialState.model = this.model;
        this.initialState.config = {
            headers: DEFAULT_HEADERS,
            buttons: {
                close: {
                    text: 'Close',
                },
            },
        };

        const initialState = this.initialState;

        this.modalRef = this.modalService.show(this.modalContentComponent, {initialState});
        this.modalRef.setClass(initialState.class);

        this.modalRef.content.updateFavs.subscribe((data) => this.returnFavs.emit(data));
    }
}
