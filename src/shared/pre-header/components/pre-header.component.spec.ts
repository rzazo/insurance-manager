import {DebugElement} from "@angular/core";
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CommonModule} from "@angular/common";
import {BsModalService, ModalModule} from "ngx-bootstrap";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

import {PreHeaderComponent} from './pre-header.component';

describe('PreHeaderComponent', () => {
    let component: PreHeaderComponent;
    let fixture: ComponentFixture<PreHeaderComponent>;
    let debugElement: DebugElement;
    let modalService: BsModalService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                FontAwesomeModule,
                ModalModule.forRoot(),
            ],
            declarations: [
                PreHeaderComponent
            ],
            providers: [
                BsModalService,
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PreHeaderComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        modalService = TestBed.get(BsModalService);

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should click button Favourites when component is rendered', () => {
        spyOn(component, 'openModal');

        let button = fixture.debugElement.nativeElement.querySelector('button');
        button.click();

        fixture.whenStable().then(() => {
            expect(component.openModal).toHaveBeenCalled();
        });
    });
});
