export interface IModalConfig {
    model?: object;
    title?: string;
    buttons?: object;
    class?: string;
    config?: object;
}