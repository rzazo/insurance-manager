import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModalModule} from 'ngx-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {
    FavouritesModalContentComponent
} from "../../elements/favourites-modal-content/components/favourites-modal-content.component";
import {PreHeaderComponent} from './components';
import {FavouritesModalContentModule} from "../../elements/favourites-modal-content/favourites-modal-content.module";

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        ModalModule.forRoot(),

        FavouritesModalContentModule
    ],
    declarations: [
        PreHeaderComponent,
    ],
    entryComponents: [
        FavouritesModalContentComponent
    ],
    exports: [
        PreHeaderComponent,
    ]
})

export class PreHeaderModule {
}
