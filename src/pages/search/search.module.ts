import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";

import {SearchComponent} from './components/search.component';
import {ViewTableModule} from "../../elements/view-table/view-table.module";
import {PreHeaderModule} from "../../shared/pre-header/pre-header.module";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,

        ViewTableModule,
        PreHeaderModule,
    ],
    exports: [
        SearchComponent,
    ],
    declarations: [
        SearchComponent
    ]
})
export class SearchModule {}
