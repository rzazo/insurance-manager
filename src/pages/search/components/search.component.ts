import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";

import {ICriteria} from "../interfaces";
import {MODEL_MOCK, FORM_VALIDATIONS} from "../constants";
import {ViewTableComponent} from "../../../elements/view-table/components/view-table.component";

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
    @ViewChild('viewTable') public viewTable: ViewTableComponent;

    public modelTable = [...MODEL_MOCK];
    public favsSelected: object[] = [];
    public form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
    ) {}

    public ngOnInit() {
        this.form = this.formBuilder.group(FORM_VALIDATIONS);

        this.form.valueChanges.subscribe((value) => {
            value = Object.keys(value)
                .filter(key => !!value[key])
                .reduce((attr, key) => {
                    attr[key] = value[key];
                    return attr
                }, {});

            this.findByCriteria(value);
        });
    }

    public onFavsSelected(data) {
        this.favsSelected = [];
        data.forEach((item) => {
            this.favsSelected.push(this.modelTable.find((element) => item === element.id));
        });
    }

    public onModalClose(data) {
        this.favsSelected = data;

        const favsIds: string[] = [];
        this.favsSelected.forEach((element) => favsIds.push(element['id']));

        this.viewTable.updateFavourite(favsIds);
    }

    private findByCriteria(criteria: ICriteria) {
        console.log('criteria', criteria);
        if (!Object.keys(criteria).length) {
            console.log('criteria IF');
            this.modelTable = [...MODEL_MOCK];
        } else {
            const keys = Object.keys(criteria);

            this.modelTable = null;
            this.modelTable = MODEL_MOCK.filter(
                (fav) => keys.every(
                    (key) => fav[key].toLowerCase().includes(criteria[key].toLowerCase())));
            console.log('criteria ELSE', this.modelTable);
        }
    }
}