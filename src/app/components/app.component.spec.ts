import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {DebugElement} from "@angular/core";
import {RouterTestingModule} from "@angular/router/testing";

import {AppComponent} from './app.component';
import {HeaderNavModule} from "../../shared/header-nav/header-nav.module";

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let debugElement: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HeaderNavModule,
                RouterTestingModule,
            ],
            declarations: [
                AppComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;

        fixture.detectChanges();
    });

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it('should exist header-nav when component is rendered', async(() => {
        let headerNav = fixture.debugElement.nativeElement.querySelector('header-nav');

        expect(headerNav).toBeTruthy();
    }));

    it('should exist router-outlet when component is rendered', async(() => {
        let routerOutlet = fixture.debugElement.nativeElement.querySelector('router-outlet');

        expect(routerOutlet).toBeTruthy();
    }));
});
