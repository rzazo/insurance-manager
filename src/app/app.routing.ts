import {Routes} from "@angular/router";

import {HomeComponent} from "../pages/home/components/home.component";
import {SearchComponent} from "../pages/search/components/search.component";

export const APP_ROUTES : Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'search', component: SearchComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '**', component: HomeComponent }
];