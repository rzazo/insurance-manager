import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';

import {APP_ROUTES} from "./app.routing";
import {HomeModule} from "../pages/home/home.module";
import {AppComponent} from './components/app.component';
import {SearchModule} from "../pages/search/search.module";
import {HeaderNavModule} from "../shared/header-nav/header-nav.module";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        NgbModule,
        BrowserModule,
        RouterModule.forRoot(APP_ROUTES),

        HomeModule,
        SearchModule,
        HeaderNavModule,
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
