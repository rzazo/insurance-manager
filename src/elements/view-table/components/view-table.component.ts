import {
    AfterViewInit,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import {faStar, faTrash} from '@fortawesome/free-solid-svg-icons';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";

import {IInsurance} from "../interfaces";
import {DEFAULT_HEADERS} from "../constants";

@Component({
    selector: 'view-table',
    templateUrl: './view-table.component.html',
    styleUrls: ['./view-table.component.scss']
})

export class ViewTableComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
    @ViewChild(MatSort) public sort;
    @ViewChild(MatPaginator) public paginator;

    @Input() public model: IInsurance[];
    @Input() public config;
    @Input() public headers: string[] = [...DEFAULT_HEADERS];

    @Output() public favs: EventEmitter<any> = new EventEmitter<any>();
    @Output() public delete: EventEmitter<any> = new EventEmitter<any>();

    public favIcon = faStar;
    public favDelete = faTrash;
    public favsList: string[] = [];
    public displayedColumns: string[];
    public dataSource: MatTableDataSource<IInsurance> = new MatTableDataSource<IInsurance>();

    public ngOnInit() {
        this.modelManager();
    }

    public ngOnChanges() {
        this.modelManager();

        if (!!this.config && this.config.delete) this.headers = [ ...DEFAULT_HEADERS, ...['delete']];
    }

    public ngOnDestroy() {
        this.dataSource.disconnect();
    }

    public ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    public setFavourite(id: string): void {
        if (this.favsList.includes(id)) this.favsList = this.favsList.filter((elementId) => elementId !== id);
        else this.favsList.push(id);

        this.favs.emit(this.favsList);
    }

    public updateFavourite(favs: string[]) {
        this.favsList = favs;
    }

    public deleteFav(id: string) {
        this.dataSource.data = this.dataSource.data.filter((element) => element.id !== id);
        this.delete.emit(id);
    }

    private modelManager() {
        this.dataSource.data = this.model;
        this.displayedColumns = this.headers;
    }
}