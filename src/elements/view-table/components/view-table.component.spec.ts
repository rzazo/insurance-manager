import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ViewTableComponent} from './view-table.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {MatCardModule, MatPaginatorModule, MatSelectModule, MatSortModule, MatTableModule} from "@angular/material";
import {DebugElement} from "@angular/core";

describe('FavouritesModalContentComponent', () => {
    let component: ViewTableComponent;
    let fixture: ComponentFixture<ViewTableComponent>;
    let debugElement: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FontAwesomeModule,

                MatSortModule,
                MatCardModule,
                MatTableModule,
                MatSelectModule,
                MatPaginatorModule,
            ],
            declarations: [
                ViewTableComponent
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ViewTableComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should load empty model when component not receive input onInit', () => {
        let model = [];

        component.dataSource.data = model;
        expect(component.dataSource.data.length).toBe(0);
    });

    it('should load model when component receive input model onChange', () => {
        let model = [
            {
                "id": "1",
                "name": "Lorem Ipsum",
                "brand": "Lorem-brand",
                "brand-image": "lorem-brand-image.png",
                "kind": "Lorem-kind",
                "Kind-image": "lorem-kind-image.png",
                "price": "100"
            },
        ];

        component.dataSource.data = model;
        expect(component.dataSource.data.length).toBe(1);
    });
});
