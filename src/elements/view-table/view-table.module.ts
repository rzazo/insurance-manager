import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatCardModule, MatPaginatorModule, MatSelectModule, MatSortModule, MatTableModule} from "@angular/material";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {ViewTableComponent} from './components/view-table.component';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        BrowserAnimationsModule,

        MatSortModule,
        MatCardModule,
        MatTableModule,
        MatSelectModule,
        MatPaginatorModule,
    ],
    declarations: [
        ViewTableComponent
    ],
    exports: [
        ViewTableComponent
    ]
})

export class ViewTableModule {
}
