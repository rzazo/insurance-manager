import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BsModalRef} from "ngx-bootstrap";

import {FavouritesModalContentComponent} from './favourites-modal-content.component';
import {ViewTableModule} from "../../view-table/view-table.module";

describe('FavouritesModalContentComponent', () => {
    let component: FavouritesModalContentComponent;
    let fixture: ComponentFixture<FavouritesModalContentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule,
                FormsModule,
                ViewTableModule,
            ],
            declarations: [
                FavouritesModalContentComponent
            ],
            providers: [
                BsModalRef,
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FavouritesModalContentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
