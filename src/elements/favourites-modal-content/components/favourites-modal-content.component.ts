import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {FormBuilder, FormGroup} from "@angular/forms";

import {ICriteria} from "../interfaces";
import {FORM_VALIDATIONS} from "../constants";

@Component({
    selector: 'favourite-modal-content',
    templateUrl: './favourites-modal-content.component.html',
    styleUrls: ['./favourites-modal-content.component.scss']
})

export class FavouritesModalContentComponent implements OnInit {
    @Input() public model;
    @Input() public config;

    @Output() updateFavs: EventEmitter<any> = new EventEmitter();

    public modelFavs;
    public title: string;
    public form: FormGroup;

    constructor(
        public bsModalRef: BsModalRef,
        private formBuilder: FormBuilder,
    ) {
    }

    public ngOnInit() {
        this.modelFavs = this.model;

        this.form = this.formBuilder.group(FORM_VALIDATIONS);
        this.form.valueChanges.subscribe((value) => {
            value = Object.keys(value)
                .filter(key => !!value[key])
                .reduce((attr, key) => {
                    attr[key] = value[key];
                    return attr
                }, {});

            (!!value)
                ? this.findByCriteria(value)
                : this.modelFavs = this.model;
        });
    }

    public findByCriteria(criteria: ICriteria) {
        const keys = Object.keys(criteria);
        this.modelFavs = this.model.filter(
            (fav) => keys.every(
                (key) => fav[key].toLowerCase().includes(criteria[key].toLowerCase())));
    }

    public deleteFav(id) {
        this.model = this.model.filter((element) => element.id !== id);
        this.modelFavs = this.model;
    }

    public onClose() {
        this.updateFavs.emit(this.modelFavs);

        this.bsModalRef.hide();
    }
}