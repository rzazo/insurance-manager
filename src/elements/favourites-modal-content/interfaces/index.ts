export interface ICriteria {
    name : string;
    brand : string;
    kind : string;
};