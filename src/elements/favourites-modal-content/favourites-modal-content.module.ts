import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";

import {ViewTableModule} from "../view-table/view-table.module";
import {FavouritesModalContentComponent} from './components/favourites-modal-content.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,

        ViewTableModule,
    ],
    entryComponents: [
        FavouritesModalContentComponent
    ],
    declarations: [
        FavouritesModalContentComponent
    ],
    exports: [
        FavouritesModalContentComponent
    ]
})

export class FavouritesModalContentModule {
}
